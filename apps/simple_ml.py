import struct
import gzip
import numpy as np

import sys
sys.path.append('python/')
import needle as ndl


def _parse_mnist_labels(filename):
    with gzip.open(filename, "rb") as f:
        buf = f.read()
    (magic_number,) = struct.unpack_from(">i", buf, offset=0)
    assert magic_number == 0x00000801, hex(magic_number)
    (num_labels,) = struct.unpack_from(">i", buf, offset=4)
    assert num_labels >= 0 and num_labels < 1e14
    return np.frombuffer(buf, dtype=">u1", count=num_labels, offset=8)


def _parse_mnist_images(filename):
    with gzip.open(filename, "rb") as f:
        buf = f.read()
    (magic_number,) = struct.unpack_from(">i", buf, offset=0)
    assert magic_number == 0x00000803, hex(magic_number)
    (num_images, num_rows, num_cols) = struct.unpack_from(">iii", buf, offset=4)
    assert num_images >= 0 and num_images < 1e14
    assert num_rows == 28 and num_cols == 28
    X_unnormalized =  np.frombuffer(buf, dtype=">u1", offset=16,
                                    count=(num_images * num_rows * num_cols),
                                   ).reshape((-1, num_rows * num_cols),
                                             order="C")
    m = np.max(X_unnormalized)
    X = X_unnormalized.astype(np.float32) / (m if m != 0 else 1)
    return X


def parse_mnist(image_filename, label_filename):
    """ Read an images and labels file in MNIST format.  See this page:
    http://yann.lecun.com/exdb/mnist/ for a description of the file format.

    Args:
        image_filename (str): name of gzipped images file in MNIST format
        label_filename (str): name of gzipped labels file in MNIST format

    Returns:
        Tuple (X,y):
            X (numpy.ndarray[np.float32]): 2D numpy array containing the loaded
                data.  The dimensionality of the data should be
                (num_examples x input_dim) where 'input_dim' is the full
                dimension of the data, e.g., since MNIST images are 28x28, it
                will be 784.  Values should be of type np.float32, and the data
                should be normalized to have a minimum value of 0.0 and a
                maximum value of 1.0.

            y (numpy.ndarray[dypte=np.int8]): 1D numpy array containing the
                labels of the examples.  Values should be of type np.int8 and
                for MNIST will contain the values 0-9.
    """
    ### BEGIN YOUR SOLUTION
    X = _parse_mnist_images(image_filename)
    y = _parse_mnist_labels(label_filename)
    assert np.shape(X)[0] == np.shape(y)[0]
    return (X, y)
    ### END YOUR SOLUTION


def mean(x: ndl.Tensor, axes=None) -> ndl.Tensor:
    if axes is None:
        return ndl.summation(x) / np.size(x.numpy())
    else:
        raise NotImplementedError()

def softmax_loss(Z, y_one_hot):
    """ Return softmax loss.  Note that for the purposes of this assignment,
    you don't need to worry about "nicely" scaling the numerical properties
    of the log-sum-exp computation, but can just compute this directly.

    Args:
        Z (ndl.Tensor[np.float32]): 2D Tensor of shape
            (batch_size, num_classes), containing the logit predictions for
            each class.
        y (ndl.Tensor[np.int8]): 2D Tensor of shape (batch_size, num_classes)
            containing a 1 at the index of the true label of each example and
            zeros elsewhere.

    Returns:
        Average softmax loss over the sample. (ndl.Tensor[np.float32])
    """
    ### BEGIN YOUR SOLUTION
    return mean(ndl.log(ndl.summation(ndl.exp(Z), axes=1))
                - ndl.summation(ndl.multiply(Z, y_one_hot), axes=1))
    ### END YOUR SOLUTION


def nn_epoch(X, y, W1, W2, lr = 0.1, batch=100):
    """ Run a single epoch of SGD for a two-layer neural network defined by the
    weights W1 and W2 (with no bias terms):
        logits = ReLU(X * W1) * W1
    The function should use the step size lr, and the specified batch size (and
    again, without randomizing the order of X).

    Args:
        X (np.ndarray[np.float32]): 2D input array of size
            (num_examples x input_dim).
        y (np.ndarray[np.uint8]): 1D class label array of size (num_examples,)
        W1 (ndl.Tensor[np.float32]): 2D array of first layer weights, of shape
            (input_dim, hidden_dim)
        W2 (ndl.Tensor[np.float32]): 2D array of second layer weights, of shape
            (hidden_dim, num_classes)
        lr (float): step size (learning rate) for SGD
        batch (int): size of SGD mini-batch

    Returns:
        Tuple: (W1, W2)
            W1: ndl.Tensor[np.float32]
            W2: ndl.Tensor[np.float32]
    """

    ### BEGIN YOUR SOLUTION
    num_examples = X.shape[0]
    (hidden_dim, num_classes) = W2.shape
    assert W1.shape[1] == hidden_dim
    for i_start in range(0, num_examples, batch):
        batch_idxs = range(i_start, min(num_examples, i_start + batch))
        X_mini_ = X[batch_idxs, :]
        y_mini_ = y[batch_idxs]
        m = X_mini_.shape[0]

        y_mini_onehot_ = np.zeros((m, num_classes))
        y_mini_onehot_[np.arange(y_mini_.size), y_mini_] = 1

        X_mini = ndl.Tensor(X_mini_)
        y_mini_onehot = ndl.Tensor(y_mini_onehot_)
        Z_mini = ndl.relu(ndl.Tensor(X_mini) @ W1) @ W2

        loss = softmax_loss(Z_mini, y_mini_onehot)
        loss.backward()
        W1 = ndl.Tensor(W1.numpy() - lr * W1.grad.numpy())
        W2 = ndl.Tensor(W2.numpy() - lr * W2.grad.numpy())
    return (W1, W2)
    ### END YOUR SOLUTION


### CODE BELOW IS FOR ILLUSTRATION, YOU DO NOT NEED TO EDIT

def loss_err(h,y):
    """ Helper function to compute both loss and error"""
    y_one_hot = np.zeros((y.shape[0], h.shape[-1]))
    y_one_hot[np.arange(y.size), y] = 1
    y_ = ndl.Tensor(y_one_hot)
    return softmax_loss(h,y_).numpy(), np.mean(h.numpy().argmax(axis=1) != y)
